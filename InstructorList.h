#include <iostream>	
#include <cstring>
#include <stdlib.h>
#include <fstream>
#include "Instructor.h"	

class InstructorList{
	void add_new_instructor();
	void delete_instructor();
	void list_all();
	void search();
	Instructor *Instructor_array;
	int currentarraysize;
public:
	int get_currentsize();
	InstructorList();
	InstructorList(int size);
	friend Instructor::Instructor(char* the_title, char* fname, char* lname, char* pnum, int* rnum, char* uname, char* email);
};

InstructorList::InstructorList(){
	this->Instructor_array = new Instructor[2];
	this->currentarraysize = 2;
}

InstructorList::InstructorList(int size){
	Instructor* newarray = new Instructor[size*2];
	int i = 0;
	for(i=0; i<size; i++){
		newarray[i]= Instructor_array[i];
	}
	this->Instructor_array = newarray;
	this->currentarraysize = size*2;
	// given size Instructor array
	// It is still a mystery that how we implement this.
}