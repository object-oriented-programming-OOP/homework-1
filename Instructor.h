#include <iostream>	
#include <cstring>
#include <stdlib.h>
#include <string>
#include <fstream> 

class Instructor{
private:
	char * title;
	char * firstname;
	char * lastname;
	char * phonenumber;
	int * roomnumber;
	char * username;
	char * emailaddress;
public:
	void display();
	void courses(char str[]);
	Instructor();
	Instructor(char* the_title, char* fname, char* lname, char* pnum, int* rnum, char* uname, char* email);
	
};
Instructor::Instructor(){
	this->title = "";
	this->firstname = "";
	this->lastname = "";
	this->phonenumber = "";
	this->roomnumber = 0;
	this->username = "";
	this->emailaddress = "";

}

Instructor::Instructor(char* the_title, char* fname, char* lname, char* pnum, int* rnum, char* uname, char* email)
{
	this->title = the_title;
	this->firstname = fname;
	this->lastname = lname;
	this->phonenumber = pnum;
	this->roomnumber = rnum;
	this->username = uname;
	this->emailaddress = email;
}