#include <iostream>	
#include <cstring>
#include <stdlib.h>
#include <fstream>
#include "Instructor.h"	
#include "InstructorList.h"

using namespace std;

class AddressBook{
	InstructorList* instanceList = new InstructorList;
	Instructor* instance1 = new Instructor;
	Instructor* instance2 = new Instructor;
	Instructor* instance3 = new Instructor;
};

void display(){

}

void InstructorList::add_new_instructor(){
	char * title;
	char * f_name;
	char * l_name;
	char * phoneno;
	char * roomno;
	char * username;
	char * email;
	cout << "Enter the title:" << endl;
	cin >> title;
	cout << "Enter the First Name:" << endl;
	cin >> f_name;
	cout << "Enter the Last Name:" << endl;
	cin >> l_name;
	cout << "Enter the Telephone Number:" << endl;
	cin >> phoneno;
	cout << "Enter the Room Number:" << endl;
	cin >> roomno;
	cout << "Enter the User Name:" << endl;
	cin >> username;
	cout << "Enter the Courses:" << endl;
	cin >> email;

}

void InstructorList::delete_instructor(){
	char * f_name;
	char * l_name;
	cout << "Enter the first name of the record to be deleted:" << endl;
	cin >> f_name;
	cout << "Enter the last name of the record to be deleted:" << endl;
	cin >> l_name;

}

void InstructorList::list_all(){
	int i = 0;
	cout << "----------List of all Instructors in Computer Engineering of ITU----------" << endl;
	cout << endl;
	for(i=0; i < this->currentarraysize; i++){
		cout << "Waiting";
	
	
	
	
	
	
	
		cout << "--------------------------------------" << endl;
	}
}

void search_by_name(){
	char * f_name;
	cout << "----------Search Results----------" << endl;
	cout << "Enter the first name:" << endl;
	cin >> f_name;
}

void search_by_surname(){

}

void search_by_phoneno(){

}

void search_by_roomno(){

}

int main(){
	bool prog_continue = true;
	while(prog_continue){
		int choice;
		cout << "1. Add a new instructor" << endl;
		cout << "2. Delete an instructor" << endl;
		cout << "3. List all instructors" << endl;
		cout << "4. Search by Name" << endl;
		cout << "5. Search by Surname" << endl;
		cout << "6. Search by Telephone Number" << endl;
		cout << "7. Search by Room Number" << endl;
		cout << "8. Exit" << endl;
		cout << "Enter the number for operation you want to perform:" << endl;  
		cin >> choice;
		switch(choice) {
		case 1: 
			void add_new_instructor(char * title, char* f_name, char* l_name, char* phoneno, char* roomno, char * username, char * email);
		case 2: 
			void delete_instructor(char *title, char* f_name, char* l_name);
		case 3: 
			void list_all(char* f_name, char* l_name, char* phoneno, char* roomno);
		case 4:
			void search_by_name();
		case 5:
			void search_by_surname();
		case 6:
			void search_by_phoneno();
		case 7:
			void search_by_roomno();
		case 8:
			return EXIT_SUCCESS;
		default: 
			cout<<"You entered a wrong value!!!"<<endl;
			break;
		}
		bool ask = true;		
		while(ask){
			char program;
			cout << "Do you want to perform another operation?(Y/N)";   cin >> program;
			if (program =='y' || program =='Y'){
				prog_continue = true;
				ask = false;
			}
			else if(program =='n' || program =='N'){
				prog_continue = false;
				ask = false;
			}
			else{
				cout << "You entered a wrong value!!!" << endl;
			}
		}	
	}
	
	
	return 0;
}